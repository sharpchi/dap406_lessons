<html>
<head>
</head>
<body>
    <form name="author" method="post">
        <div>
            <label for="id">ID:</label>
            <input type="number" name="id" value="" />
        </div>
        <div>
            <label for="firstname">Firstname:</label>
            <input type="text" name="firstname" value="" required />
        </div>
        <div>
            <label for="lastname">Lastname:</label>
            <input type="text" name="lastname" value="" required />
        </div>
        <div>
            <label for="email">Email:</label>
            <input type="email" name="email" value="" required />
        </div>
        <div>
            <label for="www">Website:</label>
            <input type="url" name="www" value="" />
        </div>
        <div>
            <label for="twitter">Twitter:</label>
            <input type="text" name="twitter" value="" />
        </div>
        <input type="submit" value="Add author" />
    </form>
</body>
</html> 