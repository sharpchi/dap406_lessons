<?php
$errors = false;
if (isset($_POST['login'])) {
    $username = isset($_POST['username']) ? trim($_POST['username']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';

    if (empty($username)) {
        $errors['username'] = 'You must enter a username.';
    }
    if (empty($password)) {
        $errors['password'] = 'You must enter a password.';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <style>
            .error {
                border: 1px #ec0817 solid;
                background-color: #f5bebf;
            }
            span.error {
                padding: 5px;
            }
            div {
                margin: 10px;
            }
        </style>
    </head>
    <body>
        <h1>Login</h1>
        <form action="" method="post">
            <div>
                <?php
                    $class = isset($errors['username']) ? 'error' : '';
                 ?>
                <label for="username">Username *:</label>
                <input type="text" name="username" value="" autocomplete="off" class="<?php echo $class; ?>" />
                <?php if (isset($errors['username'])) {
                    echo '<span class="error">' . $errors['username'] . '</span>';
                }
                ?>
            </div>
            <div>
                <?php
                    $class = isset($errors['password']) ? 'error' : '';
                 ?>
                <label for="password">Password *:</label>
                <input type="password" name="password" value="" autocomplete="off" class="<?php echo $class; ?>" />
                <?php if (isset($errors['password'])) {
                    echo '<span class="error">' . $errors['password'] . '</span>';
                }
                ?>
            </div>
            <input type="submit" name="login" value="login" />
        </form>
    </body>
</html>
