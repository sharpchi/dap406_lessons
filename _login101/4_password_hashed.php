<?php
session_start();
$loggedin = false;
$errors = [];
// http://www.xorbin.com/tools/sha256-hash-calculator
$validuser = ['username' => 'mark', 'password' => '43d9423892ab14c9da8388f7036067fa0a15b5663e563f22d3383cfc933c1690']; // mark123

if (isset($_SESSION['loggedin'])) {
    $loggedin = true;
    $username = $_SESSION['username'];

    if (isset($_POST['logout'])) {
        session_unset();
        session_destroy();
        $loggedin = false;
    }
}


if (!$loggedin) {
    if (isset($_POST['login'])) {
        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';

        if (empty($username)) {
            $errors['username'] = 'You must enter a username.';
        }
        if (empty($password)) {
            $errors['password'] = 'You must enter a password.';
        }

        if (count($errors) === 0) {
            if ($validuser['username'] == $username && $validuser['password'] == hash('sha256', $password)) {
                $loggedin = true;
                $_SESSION['loggedin'] = $loggedin;
                $_SESSION['username'] = $username;
            } else {
                $errors['failed'] = 'You have not entered correct credentials.';
            }

        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <style>
            .error {
                border: 1px #ec0817 solid;
                background-color: #f5bebf;
            }
            span.error {
                padding: 5px;
            }
            div {
                margin: 10px;
            }
        </style>
    </head>
    <body>
        <h1>Login</h1>
        <form action="" method="post">
            <?php
            if ($loggedin) {
                ?>
                <p>Hello, <?php echo $username; ?></p>
                <div>
                    <input type="submit" name="logout" value="Logout" />
                </div>
                <?php
            } else {
                if (isset($errors['failed'])) {
                    echo '<div class="error">' . $errors['failed'] . '</div>';
                }
                ?>
               <div>
                   <?php
                       $class = isset($errors['username']) ? 'error' : '';
                    ?>
                   <label for="username">Username *:</label>
                   <input type="text" name="username" value="" autocomplete="off" class="<?php echo $class; ?>" />
                   <?php if (isset($errors['username'])) {
                       echo '<span class="error">' . $errors['username'] . '</span>';
                   }
                   ?>
               </div>
               <div>
                   <?php
                       $class = isset($errors['password']) ? 'error' : '';
                    ?>
                   <label for="password">Password *:</label>
                   <input type="password" name="password" value="" autocomplete="off" class="<?php echo $class; ?>" />
                   <?php if (isset($errors['password'])) {
                       echo '<span class="error">' . $errors['password'] . '</span>';
                   }
                   ?>
               </div>
               <input type="submit" name="login" value="login" />
               <?php
            }
            ?>
        </form>
    </body>
</html>
