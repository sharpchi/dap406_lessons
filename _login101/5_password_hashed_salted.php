<?php
session_start();
$loggedin = false;
$errors = [];
// http://www.xorbin.com/tools/sha256-hash-calculator
// https://api.wordpress.org/secret-key/1.1/salt
$salt = 'mtIg6HThXFhO25x$v6 ~mIN;)fMHa[-eFRNL(xWsfC)0h+3 P0:2H*|LuR%c<Q;M';
$validuser = ['username' => 'mark', 'password' => '21f8b11e823848c010f0829c3eae5bab0211d2b4418992d89bde773666e1efe6']; // mark123

if (isset($_SESSION['loggedin'])) {
    $loggedin = true;
    $username = $_SESSION['username'];

    if (isset($_POST['logout'])) {
        session_unset();
        session_destroy();
        $loggedin = false;
    }
}


if (!$loggedin) {
    if (isset($_POST['login'])) {
        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';

        if (empty($username)) {
            $errors['username'] = 'You must enter a username.';
        }
        if (empty($password)) {
            $errors['password'] = 'You must enter a password.';
        }

        if (count($errors) === 0) {
            if ($validuser['username'] == $username && $validuser['password'] == hash('sha256', $salt . $password)) {
                $loggedin = true;
                $_SESSION['loggedin'] = $loggedin;
                $_SESSION['username'] = $username;
            } else {
                $errors['failed'] = 'You have not entered correct credentials.';
            }

        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <style>
            .error {
                border: 1px #ec0817 solid;
                background-color: #f5bebf;
            }
            span.error {
                padding: 5px;
            }
            div {
                margin: 10px;
            }
        </style>
    </head>
    <body>
        <h1>Login</h1>
        <form action="" method="post">
            <?php
            if ($loggedin) {
                ?>
                <p>Hello, <?php echo $username; ?></p>
                <div>
                    <input type="submit" name="logout" value="Logout" />
                </div>
                <?php
            } else {
                if (isset($errors['failed'])) {
                    echo '<div class="error">' . $errors['failed'] . '</div>';
                }
                ?>
               <div>
                   <?php
                       $class = isset($errors['username']) ? 'error' : '';
                    ?>
                   <label for="username">Username *:</label>
                   <input type="text" name="username" value="" autocomplete="off" class="<?php echo $class; ?>" />
                   <?php if (isset($errors['username'])) {
                       echo '<span class="error">' . $errors['username'] . '</span>';
                   }
                   ?>
               </div>
               <div>
                   <?php
                       $class = isset($errors['password']) ? 'error' : '';
                    ?>
                   <label for="password">Password *:</label>
                   <input type="password" name="password" value="" autocomplete="off" class="<?php echo $class; ?>" />
                   <?php if (isset($errors['password'])) {
                       echo '<span class="error">' . $errors['password'] . '</span>';
                   }
                   ?>
               </div>
               <input type="submit" name="login" value="login" />
               <?php
            }
            ?>
        </form>
    </body>
</html>
