<?php
session_start();
$loggedin = false;
$errors = [];
$user = [];
$DB = new PDO('mysql:dbname=dap406;host=localhost', 'root', '');
$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

// http://www.xorbin.com/tools/sha256-hash-calculator
// https://api.wordpress.org/secret-key/1.1/salt
$salt = 'mtIg6HThXFhO25x$v6 ~mIN;)fMHa[-eFRNL(xWsfC)0h+3 P0:2H*|LuR%c<Q;M';

if (isset($_SESSION['loggedin'])) {
    $loggedin = true;
    $user = $_SESSION['user'];

    if (isset($_POST['logout'])) {
        session_unset();
        session_destroy();
        $loggedin = false;
    }
}


if (!$loggedin) {
    if (isset($_POST['login'])) {
        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';

        if (empty($username)) {
            $errors['username'] = 'You must enter an email address as a username.';
        }
        if (empty($password)) {
            $errors['password'] = 'You must enter a password.';
        }

        if (count($errors) === 0) {
            $hashedpassword = hash('sha256', $salt . $password);
            $sql = "SELECT `id`, `email`, `name` FROM `user` WHERE `email`=:email AND `password`=:password";
            $stmt = $DB->prepare($sql);
            $stmt->bindValue(':email', $username, PDO::PARAM_STR);
            $stmt->bindValue(':password', $hashedpassword, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                $loggedin = true;
                $_SESSION['loggedin'] = $loggedin;
                $_SESSION['user'] = $result;
                $user = $result;
            } else {
                $errors['failed'] = 'You have not entered correct credentials.';
            }

        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <style>
            .error {
                border: 1px #ec0817 solid;
                background-color: #f5bebf;
            }
            span.error {
                padding: 5px;
            }
            div {
                margin: 10px;
            }
        </style>
    </head>
    <body>
        <h1>Login</h1>
        <form action="" method="post">
            <?php
            if ($loggedin) {
                ?>
                <p>Hello, <?php echo $user['name']; ?></p>
                <div>
                    <input type="submit" name="logout" value="Logout" />
                </div>
                <?php
            } else {
                if (isset($errors['failed'])) {
                    echo '<div class="error">' . $errors['failed'] . '</div>';
                }
                ?>
               <div>
                   <?php
                       $class = isset($errors['username']) ? 'error' : '';
                    ?>
                   <label for="username">Email *:</label>
                   <input type="email" name="username" value="" autocomplete="off" class="<?php echo $class; ?>" />
                   <?php if (isset($errors['username'])) {
                       echo '<span class="error">' . $errors['username'] . '</span>';
                   }
                   ?>
               </div>
               <div>
                   <?php
                       $class = isset($errors['password']) ? 'error' : '';
                    ?>
                   <label for="password">Password *:</label>
                   <input type="password" name="password" value="" autocomplete="off" class="<?php echo $class; ?>" />
                   <?php if (isset($errors['password'])) {
                       echo '<span class="error">' . $errors['password'] . '</span>';
                   }
                   ?>
               </div>
               <input type="submit" name="login" value="login" />
               <?php
            }
            ?>
        </form>
    </body>
</html>
