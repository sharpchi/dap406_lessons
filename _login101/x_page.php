<?php
session_start();

$loggedin = isset($_SESSION['loggedin']);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>A page</title>
    </head>
    <body>
        <h1>Another page</h1>
        <p>You are <?php echo ($loggedin) ? '' : 'not'; ?> logged in</p>
    </body>
</html>
