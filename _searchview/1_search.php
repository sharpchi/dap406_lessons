<?php
$search = isset($_POST['search']) ? trim($_POST['search']) : '';
$results = [];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Search Authors</title>
    </head>
    <body>
        <h1>Search Authors</h1>
        <form method="post">
            <div class="">
                <label for="search">Search</label>
                <input type="text" name="search" value="<?php echo $search; ?>" /> <input type="submit" name="submit" value="Search" />
            </div>
            <div class="results">
                <?php
                if (count($results) == 0 && isset($_POST['submit'])) {
                    echo '<p>No results were found.</p>';
                }
                 ?>
            </div>
        </form>
    </body>
</html>
