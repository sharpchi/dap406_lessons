<?php
$search = isset($_POST['search']) ? trim($_POST['search']) : '';
$results = [];

$DB = new PDO('mysql:dbname=dap406;host=localhost', 'root', '');
$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

if (isset($_POST['submit'])) {
    $sql = "SELECT * FROM `author` WHERE `firstname` LIKE :firstname OR `lastname` LIKE :lastname";
    $sql = "SELECT *, CONCAT(`firstname`, ' ', `lastname`) AS `fullname` FROM `author` WHERE CONCAT(`firstname`, ' ', `lastname`) LIKE :search";
    $stmt = $DB->prepare($sql);
    $stmt->bindValue(':search', '%' . $search . '%', PDO::PARAM_STR);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Search Authors</title>
    </head>
    <body>
        <h1>Search Authors</h1>
        <form method="post">
            <div class="">
                <label for="search">Search</label>
                <input type="text" name="search" value="<?php echo $search; ?>" /> <input type="submit" name="submit" value="Search" />
            </div>
            <div class="results">
                <?php
                if (count($results) == 0 && isset($_POST['submit'])) {
                    echo '<p>No results were found.</p>';
                } else if (count($results) > 0) {
                    // In html what do we use for listing items?
                    echo '<ul>';
                    foreach ($results as $item) {
                        //echo '<li>' . $item['fullname'] . '</li>';
                        echo '<li><a href="3_view.php?id=' . $item['id'] . '">' . $item['fullname'] . '</a></li>';
                    }
                    echo '</ul>';
                }
                 ?>
            </div>
        </form>
    </body>
</html>
