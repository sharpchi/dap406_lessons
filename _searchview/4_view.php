<?php

$DB = new PDO('mysql:dbname=dap406;host=localhost', 'root', '');
$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$author = [];
$errors = false;
$authorid = isset($_GET['id']) ? (int)$_GET['id'] : 0;

// Validate authorid
if (!is_numeric($authorid)) {
    $errors['id'] = 'Invalid author ID';
}

if (!$errors) {
    $sql = "SELECT * FROM `author` WHERE `id`=:id";
    $stmt = $DB->prepare($sql);
    $stmt->bindValue(':id', $authorid, PDO::PARAM_INT);
    $stmt->execute();
    $author = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$author) {
        $errors['id'] = 'Invalid author ID';
    }
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>View Author</title>
    </head>
    <body>
        <h1>Author Details</h1>
        <?php
        if (isset ($errors['id'])) {
            echo "<p>{$errors['id']}</p>";
        } else {
            ?>
            <table>
                <tr>
                    <th>ID</th>
                    <td><?php echo $author['id']; ?></td>
                </tr>
                <tr>
                    <th>Fullname</th>
                    <td><?php echo $author['firstname'] . ' ' . $author['lastname']; ?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?php echo $author['email']; ?></td>
                </tr>
                <tr>
                    <th>WWW</th>
                    <td><?php echo $author['www']; ?></td>
                </tr>
                <tr>
                    <th>Twitter</th>
                    <td><?php echo $author['twitter']; ?></td>
                </tr>
            </table>
            <a href="5_edit.php?id=<?php echo $author['id']; ?>">Edit</a>
            <?php
            $sql = "SELECT b.* FROM `book` AS b
                JOIN `book_author` AS ba ON ba.bookid=b.id
                WHERE ba.authorid=:authorid";
            $stmt = $DB->prepare($sql);
            $stmt->bindValue(':authorid', $authorid, PDO::PARAM_INT);
            $stmt->execute();
            $books = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($books) > 0) {
                ?>
                <h2>Book list</h2>
                <ul>
                    <?php
                    foreach ($books as $book) {
                        echo '<li><a href="6_book.php?id=' . $book['id'] .'">' . $book['title'] . '</a></li>';
                    }
                     ?>
                </ul>
                <?php
            } else {
                echo 'No books listed for this Author.';
            }

        }
        ?>
    </body>
</html>
