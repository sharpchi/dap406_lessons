<?php

$DB = new PDO('mysql:dbname=dap406;host=localhost', 'root', '');
$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$book = [];
$errors = false;
$bookid = isset($_GET['id']) ? (int)$_GET['id'] : 0;

// Validate authorid
if (!is_numeric($bookid)) {
    $errors['id'] = 'Invalid book ID';
}

if (!$errors) {
    $sql = "SELECT * FROM `book` WHERE `id`=:id";
    $stmt = $DB->prepare($sql);
    $stmt->bindValue(':id', $bookid, PDO::PARAM_INT);
    $stmt->execute();
    $book = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$book) {
        $errors['id'] = 'Invalid book ID';
    }
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>View Book</title>
    </head>
    <body>
        <h1>Book Details</h1>
        <?php
        if (isset ($errors['id'])) {
            echo "<p>{$errors['id']}</p>";
        } else {
            ?>
            <table>
                <tr>
                    <th>ID</th>
                    <td><?php echo $book['id']; ?></td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td><?php echo $book['title']; ?></td>
                </tr>
                <tr>
                    <th>Authors</th>
                    <td><?php echo $authors; ?></td>
                </tr>
                <tr>
                    <th>Year published</th>
                    <td><?php echo $book['yearpublished']; ?></td>
                </tr>
                <tr>
                    <th>ISBN</th>
                    <td><?php echo $book['isbn']; ?></td>
                </tr>
                <tr>
                    <th>Publisher</th>
                    <td><?php echo $book['publisher']; ?></td>
                </tr>
            </table>
            <a href="edit_book.php?id=<?php echo $book['id']; ?>">Edit book</a>
            <?php
        }
        ?>
    </body>
</html>
