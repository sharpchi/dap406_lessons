-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2017 at 11:36 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dap406`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
CREATE TABLE `author` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `www` varchar(150) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `firstname`, `lastname`, `email`, `www`, `twitter`) VALUES
(1, 'Jane', 'Seale', 'j.seale@routledge.com', 'http://www.jseale.com', '@jseale'),
(2, 'Shaun R.', 'Harper', '', '', ''),
(3, 'Stephen John', 'Quaye', 'sj@quaye.com', 'http://www.quaye.com', ''),
(4, 'Fred', 'Simmons', '', '', ''),
(9, 'Mark', 'Tadajewski', '', '', ''),
(10, 'Douglas', 'Brownlie', '', '', ''),
(11, 'Ray', 'Wright', '', '', ''),
(12, 'Dan', 'Brown', '', '', ''),
(13, 'J. K.', 'Rowling', '', '', ''),
(14, 'Stephenie', 'Meyer', '', '', ''),
(15, 'Alice', 'Sebold', '', '', ''),
(16, 'Mark', 'Haddon', '', '', ''),
(17, 'Stieg', 'Larsson', '', '', ''),
(18, 'Khaled', 'Hosseini', '', '', ''),
(19, 'Audrey', 'Niffenegger', '', '', ''),
(20, 'Jeremy', 'Clarkson', '', '', ''),
(21, 'Ian', 'McEwan', '', '', ''),
(22, 'Bill', 'Bryson', '', '', ''),
(23, 'Dave', 'Pelzer', '', '', ''),
(24, 'Alexander', 'McCall Smith', '', '', ''),
(25, 'Gillian', 'McKeith', '', '', ''),
(26, 'Tony', 'Parsons', '', '', ''),
(27, 'Sebastian', 'Faulks', '', '', ''),
(28, 'Kate', 'Mosse', '', '', ''),
(29, 'Victoria', 'Hislop', '', '', ''),
(30, 'Yann', 'Martel', '', '', ''),
(31, 'Robert C.', 'Atkins', '', '', ''),
(32, 'Louis', 'De Bernieres', '', '', ''),
(33, 'Delia', 'Smith', '', '', ''),
(34, 'Julia', 'Donaldson', '', '', ''),
(35, 'Lynne', 'Truss', '', '', ''),
(36, 'Philip', 'Pullman', '', '', ''),
(37, 'Jed', 'Rubenfeld', '', '', ''),
(38, 'Helen', 'Fielding', '', '', ''),
(39, 'Marina', 'Lewycka', '', '', ''),
(40, 'Paulo', 'Coelho', '', '', ''),
(41, 'John', 'Boyne', '', '', ''),
(42, 'Michael', 'Moore', '', '', ''),
(43, 'Jamie', 'Oliver', '', '', ''),
(44, 'John', 'Grisham', '', '', ''),
(45, 'Eric', 'Carle', '', '', ''),
(46, 'Peter', 'Kay', '', '', ''),
(47, 'Andrea', 'Levy', '', '', ''),
(48, 'Kim', 'Edwards', '', '', ''),
(49, 'Pamela', 'Stephenson', '', '', ''),
(50, 'Kate', 'Morton', '', '', ''),
(51, 'Nigella', 'Lawson', '', '', ''),
(52, 'Arthur', 'Golden', '', '', ''),
(53, 'Zadie', 'Smith', '', '', ''),
(54, 'Lauren', 'Weisberger', '', '', ''),
(55, 'Paul', 'O\'Grady', '', '', ''),
(56, 'Linwood', 'Barclay', '', '', ''),
(57, 'Joanne', 'Harris', '', '', ''),
(58, 'Frank', 'McCourt', '', '', ''),
(59, 'Ben', 'Schott', '', '', ''),
(60, 'Barack', 'Obama', '', '', ''),
(61, 'Harper', 'Lee', '', '', ''),
(62, 'J.D.', 'Salinger', '', '', ''),
(63, 'Paul', 'McKenna', '', '', ''),
(64, 'Monica', 'Ali', '', '', ''),
(65, 'Marian', 'Keyes', '', '', ''),
(66, 'Sophie', 'Kinsella', '', '', ''),
(67, 'Markus', 'Zusak', '', '', ''),
(68, 'Sharon', 'Osbourne', '', '', ''),
(69, 'Dawn', 'French', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `yearpublished` int(4) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `publisherid` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `yearpublished`, `isbn`, `publisherid`) VALUES
(3, 'E-Learning and Disability in Higher Education - Accessibility research and practice', 2006, '0414383102', 11),
(4, 'Student Engagement in Higher Education - Theoretical Perspectives and Practical Approaches for Diverse Populations', 2009, '9780415988513', 11),
(5, 'Critical Marketing', 2008, '9780470511955', 12),
(6, 'Consumer Behaviour', 2006, '1844801381', 13),
(7, 'Da Vinci Code,The', 2004, '9780552149518', 14),
(8, 'Harry Potter and the Philosopher\'s Stone#', 1997, '9780747532743', 15),
(9, 'Harry Potter and the Chamber of Secrets#', 1999, '9780747538486', 15),
(10, 'Angels and Demons#', 2003, '9780552150736', 14),
(11, 'Harry Potter and the Order of the Phoenix', 2003, '9780747551003', 15),
(12, 'Harry Potter and the Half-blood Prince:Children\'s Edition', 2005, '9780747581086', 15),
(13, 'Harry Potter and the Deathly Hallows', 2007, '9780747591054', 15),
(14, 'Harry Potter and the Prisoner of Azkaban#', 2000, '9780747546290', 15),
(15, 'Twilight#', 2007, '9781904233657', 16),
(16, 'Harry Potter and the Goblet of Fire', 2001, '9780747550990', 15),
(17, 'Deception Point#', 2004, '9780552151764', 14),
(18, 'New Moon#', 2007, '9781904233886', 16),
(19, 'Lovely Bones,The#', 2009, '9780330457729', 17),
(20, 'Digital Fortress#', 2004, '9780552151696', 14),
(21, 'Curious Incident of the Dog in the Night-time,The#', 2004, '9780099450252', 18),
(22, 'Eclipse#', 2008, '9781904233916', 16),
(23, 'Girl with the Dragon Tattoo,The:Millennium Trilogy#', 2008, '9781847245458', 19),
(24, 'Kite Runner,The#', 2004, '9780747566533', 15),
(25, 'Time Traveler\'s Wife,The', 2004, '9780099464464', 18),
(26, 'World According to Clarkson,The', 2005, '9780141017891', 20),
(27, 'Atonement#', 2002, '9780099429791', 18),
(28, 'Lost Symbol,The', 2009, '9780593054277', 21),
(29, 'Short History of Nearly Everything,A', 2004, '9780552997041', 22),
(30, 'Breaking Dawn', 2008, '9781905654284', 16),
(31, 'Girl Who Played With Fire,The:Millennium Trilogy#', 2010, '9781849163422', 23),
(32, 'Child Called It,A', 2001, '9780752837505', 24),
(33, 'No.1 Ladies\' Detective Agency,The:No.1 Ladies\' Detective Agency S.', 2003, '9780349116754', 25),
(34, 'You are What You Eat:The Plan That Will Change Your Life', 2004, '9780718147655', 26),
(35, 'Man and Boy', 2000, '9780006512134', 27),
(36, 'Birdsong', 1994, '9780099387916', 18),
(37, 'Labyrinth', 2006, '9780752877327', 24),
(38, 'Island,The', 2006, '9780755309511', 28),
(39, 'Life of Pi', 2003, '9781841953922', 29),
(40, 'Dr. Atkins\' New Diet Revolution:The No-hunger, Luxurious Weight Loss P', 2003, '9780091889487', 30),
(41, 'Tales of Beedle the Bard,The', 2008, '9780747599876', 15),
(42, 'Captain Corelli\'s Mandolin', 1995, '9780749397548', 31),
(43, 'Delia\'s How to Cook:(Bk.1) ', 1998, '9780563384304', 32),
(44, 'Gruffalo,The#', 2009, '9780330507417', 33),
(45, 'Eats, Shoots and Leaves:The Zero Tolerance Approach to Punctuation', 2003, '9781861976123', 34),
(46, 'Northern Lights:His Dark Materials S.', 1998, '9780590660549', 35),
(47, 'Interpretation of Murder,The', 2007, '9780755331420', 28),
(48, 'Girl Who Kicked the Hornets\' Nest,The:Millennium Trilogy', 2010, '9781849162746', 23),
(49, 'Bridget Jones: The Edge of Reason', 2000, '9780330367356', 17),
(50, 'Short History of Tractors in Ukrainian,A', 2006, '9780141020525', 20),
(51, 'Alchemist,The:A Fable About Following Your Dream', 1999, '9780722532935', 36),
(52, 'Notes from a Small Island', 1996, '9780552996006', 22),
(53, 'Boy in the Striped Pyjamas,The#', 2007, '9780099487821', 37),
(54, 'Stupid White Men:...and Other Sorry Excuses for the State of the Natio', 2002, '9780141011905', 20),
(55, 'Jamie\'s 30-minute Meals', 2010, '9780718154776', 26),
(56, 'Broker,The', 2005, '9780099457169', 38),
(57, 'Bridget Jones\'s Diary:A Novel', 1997, '9780330332774', 17),
(58, 'Very Hungry Caterpillar,The:The Very Hungry Caterpillar', 1994, '9780241003008', 39),
(59, 'Thousand Splendid Suns,A', 2007, '9780747582977', 15),
(60, 'Sound of Laughter,The', 2006, '9781846051616', 40),
(61, 'Jamie\'s Italy', 2005, '9780718147709', 26),
(62, 'Small Island#', 2004, '9780755307500', 28),
(63, 'Memory Keeper\'s Daughter,The', 2007, '9780141030142', 20),
(64, 'Billy Connolly', 2002, '9780007110926', 27),
(65, 'House at Riverton,The', 2007, '9780330448444', 41),
(66, 'Harry Potter and the Order of the Phoenix#', 2004, '9780747561071', 15),
(67, 'Nigella Express', 2007, '9780701181840', 42),
(68, 'Memoirs of a Geisha', 1998, '9780099771517', 18),
(69, 'Delia\'s How to Cook:(Bk.2) ', 1999, '9780563384311', 32),
(70, 'Subtle Knife,The:His Dark Materials S.', 1998, '9780590112895', 35),
(71, 'Jamie\'s Ministry of Food:Anyone Can Learn to Cook in 24 Hours', 2008, '9780718148621', 26),
(72, 'Jamie at Home:Cook Your Way to the Good Life', 2007, '9780718152437', 26),
(73, 'White Teeth', 2001, '9780140276336', 20),
(74, 'Devil Wears Prada,The', 2003, '9780007156108', 27),
(75, 'At My Mother\'s Knee ...:and Other Low Joints', 2008, '9780593059258', 21),
(76, 'No Time for Goodbye', 2008, '9780752893686', 24),
(77, 'Chocolat', 2000, '9780552998482', 22),
(78, 'Return of the Naked Chef,The', 2000, '9780718144395', 26),
(79, 'Angela\'s Ashes:A Memoir of a Childhood', 1997, '9780006498407', 43),
(80, 'Schott\'s Original Miscellany', 2002, '9780747563204', 15),
(81, 'Dreams from My Father:A Story of Race and Inheritance', 2008, '9781847670946', 29),
(82, 'To Kill a Mockingbird#', 1989, '9780099419785', 38),
(83, 'Harry Potter and the Half-blood Prince', 2005, '9780747581109', 15),
(84, 'Summons,The', 2002, '9780099406136', 38),
(85, 'Catcher in the Rye,The', 1994, '9780140237504', 20),
(86, 'I Can Make You Thin', 2005, '9780593050545', 21),
(87, 'Happy Days with the Naked Chef', 2001, '9780718144845', 26),
(88, 'Brick Lane', 2004, '9780552771153', 22),
(89, 'Anybody Out There?', 2007, '9780141019376', 20),
(90, 'Undomestic Goddess,The', 2006, '9780552772747', 22),
(91, 'Book Thief,The#', 2008, '9780552773898', 22),
(92, 'I Know You Got Soul', 2006, '9780141022925', 20),
(93, 'Sharon Osbourne Extreme:My Autobiography', 2005, '9780316731317', 44),
(94, 'Amber Spyglass,The:His Dark Materials S.', 2001, '9780439993586', 35),
(95, 'Can You Keep a Secret?', 2003, '9780552771108', 22),
(96, 'Down Under', 2001, '9780552997034', 22),
(97, 'Spot of Bother,A', 2007, '9780099506928', 18),
(98, 'Dear Fatty', 2008, '9781846053443', 40);

-- --------------------------------------------------------

--
-- Table structure for table `book_author`
--

DROP TABLE IF EXISTS `book_author`;
CREATE TABLE `book_author` (
  `id` int(10) UNSIGNED NOT NULL,
  `bookid` int(10) UNSIGNED NOT NULL,
  `authorid` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_author`
--

INSERT INTO `book_author` (`id`, `bookid`, `authorid`) VALUES
(24, 3, 1),
(25, 4, 2),
(26, 4, 3),
(30, 5, 9),
(31, 5, 10),
(32, 6, 11),
(33, 7, 12),
(34, 8, 13),
(35, 9, 13),
(36, 10, 12),
(37, 11, 13),
(38, 12, 13),
(39, 13, 13),
(40, 14, 13),
(41, 15, 14),
(42, 16, 13),
(43, 17, 12),
(44, 18, 14),
(45, 19, 15),
(46, 20, 12),
(47, 21, 16),
(48, 22, 14),
(49, 23, 17),
(50, 24, 18),
(51, 25, 19),
(52, 26, 20),
(53, 27, 21),
(54, 28, 12),
(55, 29, 22),
(56, 30, 14),
(57, 31, 17),
(58, 32, 23),
(59, 33, 24),
(60, 34, 25),
(61, 35, 26),
(62, 36, 27),
(63, 37, 28),
(64, 38, 29),
(65, 39, 30),
(66, 40, 31),
(67, 41, 13),
(68, 42, 32),
(69, 43, 33),
(70, 44, 34),
(71, 45, 35),
(72, 46, 36),
(73, 47, 37),
(74, 48, 17),
(75, 49, 38),
(76, 50, 39),
(77, 51, 40),
(78, 52, 22),
(79, 53, 41),
(80, 54, 42),
(81, 55, 43),
(82, 56, 44),
(83, 57, 38),
(84, 58, 45),
(85, 59, 18),
(86, 60, 46),
(87, 61, 43),
(88, 62, 47),
(89, 63, 48),
(90, 64, 49),
(91, 65, 50),
(92, 66, 13),
(93, 67, 51),
(94, 68, 52),
(95, 69, 33),
(96, 70, 36),
(97, 71, 43),
(98, 72, 43),
(99, 73, 53),
(100, 74, 54),
(101, 75, 55),
(102, 76, 56),
(103, 77, 57),
(104, 78, 43),
(105, 79, 58),
(106, 80, 59),
(107, 81, 60),
(108, 82, 61),
(109, 83, 13),
(110, 84, 44),
(111, 85, 62),
(112, 86, 63),
(113, 87, 43),
(114, 88, 64),
(115, 89, 65),
(116, 90, 66),
(117, 91, 67),
(118, 92, 20),
(119, 93, 68),
(120, 94, 36),
(121, 95, 66),
(122, 96, 22),
(123, 97, 16),
(124, 98, 69);

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
CREATE TABLE `publisher` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `county` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `www` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publisher`
--

INSERT INTO `publisher` (`id`, `name`, `address1`, `address2`, `town`, `county`, `country`, `postcode`, `phone`, `www`, `twitter`) VALUES
(1, 'Penguin', '1 Fish Street', '', '', 'London', 'GB', 'SE1 1EE', '020 7555 5555', 'http://www.penguin.com', '@penguin'),
(11, 'Routledge', '2 Park Square', 'Milton Park', 'Abingdon', 'Oxfordshire', 'GB', 'OX14 4RN', '', 'http://www.routledge.com', '@routledgebooks'),
(12, 'John Wiley & Sons', 'The Atrium', 'Southern Gate', 'Chichester', 'West Sussex', 'GB', 'PO19 8SQ', '01243 779777', 'http://www.wiley.com', '@wileyglobal'),
(13, 'Thomson', 'High Holborn House', '60-51 Bedford Row', 'London', '', 'GB', 'WC1R 4LR', '', 'http://www.thomsonlearning.co.uk', ''),
(14, 'Corgi Books', '', '', '', '', '', '', '', '', ''),
(15, 'Bloomsbury Publishing PLC', '', '', '', '', '', '', '', '', ''),
(16, 'ATOM', '', '', '', '', '', '', '', '', ''),
(17, 'Picador', '', '', '', '', '', '', '', '', ''),
(18, 'Vintage', '', '', '', '', '', '', '', '', ''),
(19, 'Quercus', '', '', '', '', '', '', '', '', ''),
(20, 'Penguin Books', '', '', '', '', '', '', '', '', ''),
(21, 'Bantam Press', '', '', '', '', '', '', '', '', ''),
(22, 'Black Swan', '', '', '', '', '', '', '', '', ''),
(23, 'Quercus Publishing Plc', '', '', '', '', '', '', '', '', ''),
(24, 'Orion (an Imprint of The Orion', '', '', '', '', '', '', '', '', ''),
(25, 'Abacus', '', '', '', '', '', '', '', '', ''),
(26, 'Michael Joseph', '', '', '', '', '', '', '', '', ''),
(27, 'HarperCollins Publishers', '', '', '', '', '', '', '', '', ''),
(28, 'Headline Review', '', '', '', '', '', '', '', '', ''),
(29, 'Canongate Books', '', '', '', '', '', '', '', '', ''),
(30, 'Vermilion', '', '', '', '', '', '', '', '', ''),
(31, 'Minerva', '', '', '', '', '', '', '', '', ''),
(32, 'BBC Books (Random House)', '', '', '', '', '', '', '', '', ''),
(33, 'Macmillan Children\'s Books', '', '', '', '', '', '', '', '', ''),
(34, 'Profile Books', '', '', '', '', '', '', '', '', ''),
(35, 'Point', '', '', '', '', '', '', '', '', ''),
(36, 'Thorsons', '', '', '', '', '', '', '', '', ''),
(37, 'Definitions', '', '', '', '', '', '', '', '', ''),
(38, 'Arrow Books', '', '', '', '', '', '', '', '', ''),
(39, 'Puffin Books', '', '', '', '', '', '', '', '', ''),
(40, 'Century', '', '', '', '', '', '', '', '', ''),
(41, 'Pan Books', '', '', '', '', '', '', '', '', ''),
(42, 'Chatto & Windus', '', '', '', '', '', '', '', '', ''),
(43, 'Flamingo', '', '', '', '', '', '', '', '', ''),
(44, 'Time Warner Books', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` char(64) NOT NULL COMMENT 'SHA256 - 4bits per char - fixed length',
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `author_name` (`firstname`,`lastname`) USING BTREE;

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_title` (`title`),
  ADD KEY `fk_book_publisher` (`publisherid`);

--
-- Indexes for table `book_author`
--
ALTER TABLE `book_author`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `book_author` (`bookid`,`authorid`) USING BTREE,
  ADD KEY `fk_ba_author` (`authorid`);

--
-- Indexes for table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `publisher_name` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `book_author`
--
ALTER TABLE `book_author`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `fk_book_publisher` FOREIGN KEY (`publisherid`) REFERENCES `publisher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
