CREATE TABLE `user` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `email` varchar(100) NOT NULL,
 `password` char(64) NOT NULL COMMENT 'SHA256 - 4bits per char - fixed length',
 `name` varchar(50) NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `idx_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
